package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;

import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;

/**
 * Tests for the calcNextSchduleDate() method.
 * 
 * @author Kathryn Browning
 * @version November 29, 2015
 *
 */
public class WhenCalculatingNextLectureDate {

	/**
	 * Test that the hash map contains all of the days of the week.
	 */
	@Test
	public void hashMapShouldContainAllDaysOfWeek() {
	}

	/**
	 * Test that the hash map has the correct size.
	 */
	@Test
	public void hashMapShouldHaveSizeOf7() {

	}

	/**
	 * Test that the lecture period date should change appropriately.
	 */
	@Test
	public void lecturePeriodDateShouldChange() {
		CourseSchedule schedule = new CourseSchedule();
		schedule.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		schedule.calcNextScheduleDate(LocalDate.of(2015, 11, 04));
		schedule.wednesdayProperty().set(true);
		assertEquals(LocalDate.of(2015, 11, 04), schedule.get(0).dateProperty().getValue());
	}

	/**
	 * Test that multiple lecture periods change date.
	 */
	@Test
	public void multipleLecturePeriodsShouldChangeDate() {

	}

}
