/**
 * 
 */
package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;

import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;

/**
 * Test class for adjusting the schedule.
 * 
 * @author Kathryn Browning
 * @version November 15, 2015
 *
 */
public class WhenAdjustingSchedules {

	/**
	 * Test that the startDateProperty accessor works as expected.
	 */
	@Test
	public void shouldGetStartDate() {
		CourseSchedule schedule = new CourseSchedule(LocalDate.of(2015, 11, 24)); 
		assertEquals(LocalDate.of(2015, 11, 24), schedule.startDateProperty().getValue());
	}

	/**
	 * Test that the first LecturePeriod in the list has the initial start date.
	 */
	@Test
	public void firstLectureShouldBePickedStartDate() {
		CourseSchedule schedule = new CourseSchedule(LocalDate.of(2015, 11, 24));
		schedule.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		schedule.updateAllDates(LocalDate.of(2015, 11, 24));
		assertEquals(LocalDate.of(2015, 11, 24), schedule.get(0).dateProperty().getValue());
	}

	/**
	 * Test that a middle LecturePeriod object has the correct date.
	 */
	@Test
	public void middleLecturesShouldHaveCorrecteDates() {
		CourseSchedule schedule = new CourseSchedule(LocalDate.of(2015, 11, 24));
		schedule.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		schedule.updateAllDates(LocalDate.of(2015, 11, 24));
		assertEquals(LocalDate.of(2015, 11, 26), schedule.get(2).dateProperty().getValue());
	}

	/**
	 * Test that the last LecturePeriod in the list has the correct date.
	 */
	@Test
	public void lastLectureInListShouldHaveCorrectDate() {
		CourseSchedule schedule = new CourseSchedule(LocalDate.of(2015, 11, 24));
		schedule.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		schedule.updateAllDates(LocalDate.of(2015, 11, 24));
		assertEquals(LocalDate.of(2015, 11, 27), schedule.get(3).dateProperty().getValue());
	}

	
	/**
	 * Test that multiple LecturePeriods have the correct dates.
	 */
	@Test
	public void multipleLecturePeriodsShouldHaveCorrectDates() {
		CourseSchedule schedule = new CourseSchedule(LocalDate.of(2015, 11, 24));
		schedule.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		schedule.updateAllDates(LocalDate.of(2015, 11, 24));
		assertEquals(LocalDate.of(2015, 11, 24), schedule.startDateProperty().getValue());
		assertEquals(LocalDate.of(2015, 11, 24), schedule.get(0).dateProperty().getValue());
		assertEquals(LocalDate.of(2015, 11, 25), schedule.get(1).dateProperty().getValue());
		assertEquals(LocalDate.of(2015, 11, 26), schedule.get(2).dateProperty().getValue());
		assertEquals(LocalDate.of(2015, 11, 27), schedule.get(3).dateProperty().getValue());
	}
}
