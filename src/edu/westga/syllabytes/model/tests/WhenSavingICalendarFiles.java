package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import edu.westga.syllabytes.model.CourseSchedule;
import net.fortuna.ical4j.model.ValidationException;

/**
 * Tests for saving ICS files
 * 
 * @author Kathryn Browning
 * @version December 5, 2015
 *
 */
public class WhenSavingICalendarFiles {

	/**
	 * Temporary folder to store testing files.
	 */
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	/**
	 * Test that the file can be saved.
	 * 
	 * @throws IOException
	 *             if the input/output operation cannot be performed.
	 * @throws ValidationException
	 *             if the generated file does not have a valid iCalendar format.
	 */
	@Test
	public void shouldSaveFile() throws IOException, ValidationException {
		CourseSchedule schedule1 = new CourseSchedule();
		schedule1.loadFromFile(new File("SyllabytesTestCal_nl3hvl8e3nilb9dnnqasgqg6hk@group.calendar.google.com.ics"));
		File courseScheduleFile = this.tempFolder
				.newFile("SyllabytesTestCal_nl3hvl8e3nilb9dnnqasgqg6hk@group.calendar.google.com.ics");
		schedule1.saveFile(courseScheduleFile);

		CourseSchedule schedule2 = new CourseSchedule();
		schedule2.loadFromFile(courseScheduleFile);

		assertEquals(5, schedule2.size());
	} 
}
