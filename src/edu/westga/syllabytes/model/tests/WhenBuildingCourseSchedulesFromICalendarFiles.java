package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.LecturePeriod;

/**
 * Test cases to make sure a course schedule and lecture period are created
 * correctly.
 * 
 * @author Kathryn Browning
 * @version November 1, 2015
 *
 */
public class WhenBuildingCourseSchedulesFromICalendarFiles {
	private CourseSchedule schedule;

	/**
	 * Set up
	 * 
	 * @throws Exception
	 *             the exception thrown
	 */
	@Before
	public void setUp() throws Exception {
		this.schedule = new CourseSchedule();
	}

	/**
	 * Test that a lecture period is added to the schedule.
	 */
	@Test
	public void shouldAddLecturePeriodToSchedule() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));

		LecturePeriod lecture = new LecturePeriod("Vet Appointment", "Vet Appointment for Dex",
				LocalDate.now().plus(1, ChronoUnit.DAYS));
		this.schedule.add(1, lecture);
		assertEquals(5, this.schedule.size());
	}

	/**
	 * Should be able to read a file without any exceptions being thrown.
	 */
	@Test
	public void shouldReadFileWithoutExceptionsThrown() {
		this.schedule.loadFromFile(new File("kathryn.e.browning@gmail.com.ics"));
	}

	/**
	 * Test that a lecture period can be removed from the course schedule.
	 */
	@Test
	public void shouldRemoveLecturePeriodFromSchedule() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		this.schedule.remove(1);
		assertEquals(3, this.schedule.size());
	}

	/**
	 * Test that a lecture period can be set in the course schedule.
	 */
	@Test
	public void shouldSetLecturePeriodInSchedule() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		LecturePeriod lecture = new LecturePeriod("Vet Appointment", "Vet Appointment for Dex",
				LocalDate.now().plus(1, ChronoUnit.DAYS));
		this.schedule.set(3, lecture);
		assertEquals(4, this.schedule.size());
	}

	/**
	 * Test that you can get a lecture period from the course schedule.
	 */
	@Test
	public void shouldBeAbleToGetLecturePeriodFromSchedule() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		this.schedule.get(2);
	}

	/**
	 * Test that you can get the size of the course schedule.
	 */
	@Test
	public void shouldBeAbleToGetSizeOfCourseSchedule() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		assertEquals(4, this.schedule.size());
	}

	/**
	 * Test that the getSchedule() method runs correctly.
	 */
	@Test
	public void shouldBeAbleToGetEntireSchedule() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar1_goramd7c16ntf2fcnm59e4aln0@group.calendar.google.com.ics"));
		this.schedule.getSchedule();
	}

	/**
	 * Test that recurring events are treated as a single lecture period object
	 * each time the event recurs.
	 */
	@Test
	public void shouldCreateMultipleLecturePeriodsForRecurringEvents() {
		this.schedule
				.loadFromFile(new File("iCalTestCalendar2_sihglfidaq9a6kh4u9jjrm1i60@group.calendar.google.com.ics"));
		assertEquals(20, this.schedule.size());
	}

}
