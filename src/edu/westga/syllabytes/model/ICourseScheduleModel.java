package edu.westga.syllabytes.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import net.fortuna.ical4j.model.ValidationException;

/**
 * Interface for the CourseSchedule.
 * 
 * @author Kathryn Browning
 * @version December 5, 2015
 *
 */
public interface ICourseScheduleModel {

	/**
	 * Property accessor corresponding to the VEvent's start date.
	 * 
	 * @return the start date.
	 */
	ObjectProperty<LocalDate> startDateProperty();

	/**
	 * Property accessor for the contained LecturePeriod objects.s
	 * 
	 * @return a list of LecturePeriod objects
	 */
	ObservableList<LecturePeriod> getSchedule();

	/**
	 * True if course meets on Sundays.
	 * 
	 * @return true if the course meets on Sundays during the semester
	 */
	BooleanProperty sundayProperty();

	/**
	 * True if course meets on Mondays.
	 * 
	 * @return true if the course meets on Mondays during the semester
	 */
	BooleanProperty mondayProperty();

	/**
	 * True if course meets on Tuesdays.
	 * 
	 * @return true if the course meets on Tuesdays during the semester
	 */
	BooleanProperty tuesdayProperty();
 
	/**
	 * True if course meets on Wednesdays.
	 * 
	 * @return true if the course meets on Wednesdays during the semester
	 */
	BooleanProperty wednesdayProperty();

	/**
	 * True if course meets on Thursdays.
	 * 
	 * @return true if the course meets on Thursdays during the semester
	 */
	BooleanProperty thursdayProperty();

	/**
	 * True if course meets on Friday.
	 * 
	 * @return true if the course meets on Fridays during the semester
	 */
	BooleanProperty fridayProperty();

	/**
	 * True if course meets on Saturdays.
	 * 
	 * @return true if the course meets on Saturdays during the semester
	 */
	BooleanProperty saturdayProperty();

	/**
	 * Load an ICS File and store the LecturePeriods in a list.
	 * 
	 * @param icalFile
	 *            the incoming calendar file
	 */
	void loadFromFile(File icalFile);

	/**
	 * Saves the current ICourseScheduleMOdel schedule as an ICS file.
	 * 
	 * @param icalFile
	 *            the icalFile to be saved
	 * @throws FileNotFoundException
	 *             if the file specified is unable to be opened
	 * @throws IOException
	 *             if something has gone wrong with the input/output operation
	 * @throws ValidationException
	 *             if the generated file does not have a valid iCalendar format.
	 */
	void saveFile(File icalFile) throws FileNotFoundException, IOException, ValidationException;
}
