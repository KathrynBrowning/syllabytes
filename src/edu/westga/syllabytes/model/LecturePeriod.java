/**
 * 
 */
package edu.westga.syllabytes.model;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Create a lecture period object with a title, description, and date.
 * 
 * @author Kathryn Browning
 * @version October 27, 2015
 *
 */
public class LecturePeriod {
	private final StringProperty title;
	private final StringProperty description;
	private final ObjectProperty<LocalDate> date;

	/**
	 * Creates a new LecturePeriod with the given title, description, and date.
	 * 
	 * @param ttl
	 *            the title of the event
	 * @param dscrptn
	 *            the description of the event
	 * @param dt
	 *            the date of the event
	 */
	public LecturePeriod(String ttl, String dscrptn, LocalDate dt) {
		this.title = new SimpleStringProperty(ttl);
		this.description = new SimpleStringProperty(dscrptn);
		this.date = new SimpleObjectProperty<LocalDate>(dt);
	}

	/**
	 * Property accessor corresponding to VEVENT's summary.
	 * 
	 * @return the title
	 */
	public StringProperty titleProperty() {
		return this.title;
	}

	/**
	 * Property accessor corresponding to VEVENT's description.
	 * 
	 * @return the description
	 */
	public StringProperty descriptionProperty() {
		return this.description;
	}

	/**
	 * Property accessor corresponding to VEVENT's start date.
	 * 
	 * @return the date
	 */
	public ObjectProperty<LocalDate> dateProperty() {
		return this.date;
	}
}
