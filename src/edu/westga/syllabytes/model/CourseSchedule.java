/**
 *  
 */
package edu.westga.syllabytes.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.RRule;

/**
 * Creates a course schedule by reading in a calendar file and creating lectures
 * based on the events in the file.
 * 
 * @author Kathryn Browning
 * @version December 5, 2015
 *
 */
public class CourseSchedule extends ModifiableObservableListBase<LecturePeriod> implements ICourseScheduleModel {
	private final ObservableList<LecturePeriod> lecturePeriodList = FXCollections.observableArrayList();
	private ArrayList<VEvent> vEventList;
	private ObjectProperty<LocalDate> date;
	private BooleanProperty isOnDate;
	private Calendar calendar;

	/**
	 * Constructor
	 */
	public CourseSchedule() {
		this.vEventList = new ArrayList<VEvent>();
		this.isOnDate = new SimpleBooleanProperty();
		this.calendar = new Calendar();
	}

	/**
	 * Constructor
	 * 
	 * @param dt
	 *            the start date
	 */
	public CourseSchedule(LocalDate dt) {
		this();
		this.date = new SimpleObjectProperty<LocalDate>(dt);
	}

	@Override
	public ObservableList<LecturePeriod> getSchedule() {
		return this.lecturePeriodList;
	}

	@Override
	protected void doAdd(int index, LecturePeriod lecture) {
		this.lecturePeriodList.add(index, lecture);
	}

	@Override
	protected LecturePeriod doRemove(int index) {
		return this.lecturePeriodList.remove(index);
	}

	@Override
	protected LecturePeriod doSet(int index, LecturePeriod lecture) {
		return this.lecturePeriodList.set(index, lecture);
	}

	@Override
	public LecturePeriod get(int index) {
		return this.lecturePeriodList.get(index);
	}

	@Override
	public int size() {
		return this.lecturePeriodList.size();
	}

	@Override
	public ObjectProperty<LocalDate> startDateProperty() {
		return this.date;
	}

	@Override
	public BooleanProperty sundayProperty() {
		return this.isOnDate;
	}

	@Override
	public BooleanProperty mondayProperty() {
		return this.isOnDate;
	}

	@Override
	public BooleanProperty tuesdayProperty() {
		return this.isOnDate;
	}

	@Override
	public BooleanProperty wednesdayProperty() {
		return this.isOnDate;
	}

	@Override
	public BooleanProperty thursdayProperty() {
		return this.isOnDate;
	}

	@Override
	public BooleanProperty fridayProperty() {
		return this.isOnDate;
	}

	@Override
	public BooleanProperty saturdayProperty() {
		return this.isOnDate;
	}

	@Override
	public void loadFromFile(File icalFile) {
		CalendarBuilder calBuilder = new CalendarBuilder();
		try {
			this.calendar = calBuilder.build(new FileInputStream(icalFile));
			if (this.calendar == null) {
				throw new IllegalStateException("Calendar cannot be null.");
			}
			calBuilder.getRegistry();
			this.turnVEventToLecturePeriod(this.calendar);
		} catch (IOException ioException) {
			System.out.println("Input operation has failed.");
		} catch (ParserException parserException) {
			System.out.println("Error while parsing input file.");
		}
	}

	@Override
	public void saveFile(File icalFile) throws FileNotFoundException, IOException, ValidationException {
		try {
			FileOutputStream outputFile = new FileOutputStream(icalFile);
			CalendarOutputter calOutputter = new CalendarOutputter();
			calOutputter.output(this.calendar, outputFile);
		} catch (FileNotFoundException fileNotFoundException) {
			System.out.println("The file is unable to be opened.");
		} catch (IOException ioException) {
			System.out.println("Output operation has failed.");
		} catch (ValidationException validationException) {
			System.out.println("Generated file does not have a valid iCalendar format.");
		}
	}

	/**
	 * Changes the date of all LecturePeriod objects in the schedule so that
	 * they are on consecutive days.
	 * 
	 * @param startDate
	 *            the start date selected by the user.
	 */
	public void updateAllDates(LocalDate startDate) {
		this.date = new SimpleObjectProperty<LocalDate>(startDate);
		LocalDate sDate = this.date.getValue();

		for (int i = 1; i < this.lecturePeriodList.size(); i++) {
			sDate = sDate.plusDays(1);

			LecturePeriod lecturePeriodStart = this.lecturePeriodList.get(0);
			lecturePeriodStart.dateProperty().setValue(startDate);

			LecturePeriod lecturePeriod = this.lecturePeriodList.get(i);
			lecturePeriod.dateProperty().setValue(sDate);
		}
	}

	/**
	 * This method determines the first date on or after a given date that falls
	 * on one of the days of the week in which the course meets.
	 * 
	 * @param date
	 *            the start date chosen by the user
	 * @return the new date the lecture is on
	 */
	public LocalDate calcNextScheduleDate(LocalDate date) {
		HashMap<DayOfWeek, BooleanProperty> dateMatching = new HashMap<>();
		dateMatching.put(DayOfWeek.SUNDAY, this.sundayProperty());
		dateMatching.put(DayOfWeek.MONDAY, this.mondayProperty());
		dateMatching.put(DayOfWeek.TUESDAY, this.tuesdayProperty());
		dateMatching.put(DayOfWeek.WEDNESDAY, this.wednesdayProperty());
		dateMatching.put(DayOfWeek.THURSDAY, this.thursdayProperty());
		dateMatching.put(DayOfWeek.FRIDAY, this.fridayProperty());
		dateMatching.put(DayOfWeek.SATURDAY, this.saturdayProperty());

		for (int i = 0; i < dateMatching.size(); i++) {
			if (dateMatching.containsKey(date.getDayOfWeek()) && dateMatching.get(date.getDayOfWeek()).get()) {
				for (LecturePeriod lecture : this.lecturePeriodList) {
					lecture.dateProperty().setValue(date);
				}
			}
		}
		return date;
	}

	/**
	 * Iterates through the VEvents and adds them to an array list which is then
	 * gone through to get the title, description, and start date of each event.
	 * The tile, description, and start date are then used to build a
	 * LecturePeriod object.
	 * 
	 * @param calendar
	 *            the calendar object
	 * @return a LecturePeriod object based on the VEvent title, description,
	 *         and start date
	 */
	private void turnVEventToLecturePeriod(Calendar calendar) {

		PriorityQueue<VEvent> event = new PriorityQueue<VEvent>(1, new Comparator<VEvent>() {
			@Override
			public int compare(VEvent event1, VEvent event2) {
				Date startDate1 = event1.getStartDate().getDate();
				Date startDate2 = event2.getStartDate().getDate();

				Instant instant1 = Instant.ofEpochMilli(startDate1.getTime());
				LocalDate localDate1 = LocalDateTime.ofInstant(instant1, ZoneId.systemDefault()).toLocalDate();

				Instant instant2 = Instant.ofEpochMilli(startDate2.getTime());
				LocalDate localDate2 = LocalDateTime.ofInstant(instant2, ZoneId.systemDefault()).toLocalDate();

				return localDate1.compareTo(localDate2);
			}
		});

		this.iterateThroughVEvents(this.vEventList, calendar);

		event.addAll(this.vEventList);

		this.createLecturePeriod();
	}

	/**
	 * Iterate through the VEvents stored in a calendar and add them to an
	 * ArrayList.
	 * 
	 * @param vEventList
	 *            the ArrayList of VEvents
	 * @param calendar
	 *            the Calendar
	 */
	private void iterateThroughVEvents(ArrayList<VEvent> vEventList, Calendar calendar) {
		@SuppressWarnings("unchecked")
		Iterator<VEvent> vEvent = (Iterator<VEvent>) calendar.getComponents(Component.VEVENT).iterator();
		while (vEvent.hasNext()) {
			vEventList.add((VEvent) vEvent.next());
		}
	}

	/**
	 * Go through each VEvent and create a LecturePeriod object based on the
	 * VEvent's summary, description, and start date. Then add the LecturePeriod
	 * objects to the schedule.
	 * 
	 * @param vEventList
	 *            the ArrayList containing all VEvents
	 */
	private void createLecturePeriod() {
		LecturePeriod lecture = null;
		for (VEvent event : this.vEventList) {
			this.checkForRecurrance(event);
			String title = event.getSummary().getValue();
			String description = event.getDescription().getValue();
			Date date = event.getStartDate().getDate();

			Instant instant = Instant.ofEpochMilli(date.getTime());
			LocalDate startDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();

			lecture = new LecturePeriod(title, description, startDate);

			this.lecturePeriodList.add(lecture);
		}
	}

	/**
	 * If a VEvent has recurrence, create a lecture period off of each
	 * recurrence.
	 * 
	 * @param event
	 *            the vEvent that is being checked for recurrence
	 */
	private void checkForRecurrance(VEvent event) {
		RRule rule = (RRule) event.getProperty(Property.RRULE);
		LecturePeriod lecture = null;
		if (rule != null) {
			for (int i = 1; i < rule.getRecur().getCount(); i++) {
				String title = event.getSummary().getValue();
				String description = event.getDescription().getValue();
				Date date = event.getStartDate().getDate();

				Instant instant = Instant.ofEpochMilli(date.getTime());
				LocalDate startDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();

				lecture = new LecturePeriod(title, description, startDate);

				this.lecturePeriodList.add(lecture);
			}
		}
	}
}
