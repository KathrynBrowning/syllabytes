/**
 * 
 */
package edu.westga.syllabytes.views;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.ICourseScheduleModel;
import edu.westga.syllabytes.model.LecturePeriod;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.fortuna.ical4j.model.ValidationException;

/**
 * Controller class associated with SyllabytesMainView.fxml
 * 
 * @author Kathryn Browning
 * @version December 6, 2015
 *
 */
public class SyllabytesMainViewController {

	private ObservableList<LecturePeriod> lecturePeriodList = FXCollections.observableArrayList();
	private ICourseScheduleModel schedule;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TableView<LecturePeriod> lecturePeriodScheduleView;

	@FXML
	private TableColumn<LecturePeriod, LocalDate> dateColumn;

	@FXML
	private TableColumn<LecturePeriod, String> titleColumn;

	@FXML
	private TableColumn<LecturePeriod, String> descriptionColumn;

	@FXML
	private DatePicker startDatePicker;

	@FXML
	private Button selectDate;

	@FXML
	private CheckBox sunday;

	@FXML
	private CheckBox monday;

	@FXML
	private CheckBox tuesday;

	@FXML
	private CheckBox wednesday;

	@FXML
	private CheckBox thursday;

	@FXML
	private CheckBox friday;

	@FXML
	private CheckBox saturday;

	@FXML
	private MenuBar menuBar;

	@FXML
	private Menu fileMenu;

	@FXML
	private MenuItem openMenu;

	@FXML
	private MenuItem saveMenu;

	@FXML
	void initialize() {
		assert this.lecturePeriodScheduleView != null : "fx:id=\"lecturePeriodScheduleView\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.dateColumn != null : "fx:id=\"dateColumn\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.titleColumn != null : "fx:id=\"titleColumn\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.descriptionColumn != null : "fx:id=\"descriptionColumn\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.startDatePicker != null : "fx:id=\"startDate\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.selectDate != null : "fx:id=\"selectDate\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.sunday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.monday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.tuesday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.wednesday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.thursday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.friday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.saturday != null : "fx:id=\"sunday\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.menuBar != null : "fx:id=\"menuBar\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.fileMenu != null : "fx:id=\"fileMenu\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.openMenu != null : "fx:id=\"openMenu\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";
		assert this.saveMenu != null : "fx:id=\"saveMenu\" was not injected : check your FXML file 'SyllabytesMainView.fxml'.";

		this.initCalendarData();
		this.changeStartDate();
		this.changeLectureDayOfWeek();
		this.menuFunctionality();
	}

	/**
	 * Assign data to the different calendar pieces.
	 */
	private void initCalendarData() {
		this.dateColumn.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
		this.titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
		this.lecturePeriodList = this.lecturePeriodScheduleView.getItems();

		this.addLecturePeriodsFromFile();
	}

	/**
	 * Add LecturePeriods from an ICS file to the lecturePeriodList.
	 */
	private void addLecturePeriodsFromFile() {
		this.schedule = new CourseSchedule();
		this.schedule
				.loadFromFile(new File("SyllabytesTestCal_nl3hvl8e3nilb9dnnqasgqg6hk@group.calendar.google.com.ics"));
		for (LecturePeriod lecture : this.schedule.getSchedule()) {
			this.lecturePeriodList.add(lecture);
		}
	}

	/**
	 * If button is pressed, change start date to the date picked.
	 */
	private void changeStartDate() {
		this.selectDate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				schedule.getSchedule();
				((CourseSchedule) schedule).updateAllDates(startDatePicker.getValue());
				dateColumn.setCellValueFactory(cellData -> schedule.startDateProperty());
			}
		});
	}

	/**
	 * If a day of the week checkbox is checked, immediately change the lecture
	 * period list so that the lecture period dates are on that specific day of
	 * the week.
	 */
	private void changeLectureDayOfWeek() {
		ArrayList<CheckBox> daysOfWeek = new ArrayList<CheckBox>();
		daysOfWeek.add(this.sunday);
		daysOfWeek.add(this.monday);
		daysOfWeek.add(this.tuesday);
		daysOfWeek.add(this.wednesday);
		daysOfWeek.add(this.thursday);
		daysOfWeek.add(this.friday);
		daysOfWeek.add(this.saturday);

		CourseSchedule schedule = new CourseSchedule();
		for (CheckBox day : daysOfWeek) {
			if (day.isPressed()) {
				schedule.calcNextScheduleDate(this.startDatePicker.getValue());
			}
		}
	}

	private void menuFunctionality() {
		this.openMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				FileChooser fileChooser = new FileChooser();
				File icalFile =	fileChooser.showOpenDialog(new Stage());
				if (icalFile != null) {
					schedule.loadFromFile(icalFile);
				}
			}
		});
		this.saveMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				try {
					FileChooser fileChooser = new FileChooser();
					File icalFile = fileChooser.showSaveDialog(new Stage());
					configureFileExtension(fileChooser);
					if (icalFile != null) {
						schedule.saveFile(icalFile);
					}
				} catch (IOException | ValidationException exception) {
					System.out.println("Output operation has failed or generated file does not have correct format.");
				}
			}
		});
	}
	
	private void configureFileExtension(FileChooser fileChooser) {
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("iCalendar", "*.ics"));
	}
}
