/**
 * 
 */
package edu.westga.syllabytes;

import java.net.URL;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Main class to run application.
 * 
 * @author Kathryn Browning
 * @version November 7, 2015
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource("SyllabytesMainView.fxml");
		FXMLLoader loader = new FXMLLoader(resource);
		Parent root = (Parent) loader.load();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            the command line argument passed to the application.
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
